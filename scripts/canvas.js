var canvas = (function() {
    'use strict';

    var init = function() {

        var cube = {};
        var cubeIndex = 0;
        var cubeNmb = 150;
        var scene = new THREE.Scene();
        var camera = new THREE.PerspectiveCamera( 85, window.innerWidth / window.innerHeight, 0.1, 1000 );

        var controls = new THREE.OrbitControls( camera );


        //    scene.fog = new THREE.Fog('white', 200, 205);

        var renderer = new THREE.WebGLRenderer({alpha:true,antialias:true});
        var container =  document.querySelector('.cube');

        function Cube() {

            var size = 10;
            var radius = ( Math.random() - 0.5 ) * 3;

            cubeIndex % 2 === 0 ? this.geometry = new THREE.SphereGeometry( radius, size, size) : this.geometry = new THREE.BoxGeometry( radius, radius, radius);

            cubeIndex % 2 === 0 ? this.material = new THREE.MeshPhongMaterial( {color: '#B16286' } ) : this.material = new THREE.MeshPhongMaterial( {color: '#458588' } )

            // this.material = new THREE.MeshPhongMaterial( {color: 'black' } );
            this.c = new THREE.Mesh( this.geometry, this.material );

            this.c.position.x = ( Math.random() - 0.5 ) * 500;
            this.c.position.y = ( Math.random() - 0.5 ) * 500;
            this.c.position.z = ( Math.random() - 0.5 ) * 500;

            this.id = cubeIndex;
            this.c.name = cubeIndex;
            this.life = 0;
            this.maxLife = 300;
            this.speed = ( Math.random() - 0.5 ) * 3;

            // cubeIndex % 5 === 0 ? this.material.color.setHex( '0x490a3d' ) : cubeIndex % 9 === 0 ? this.material.color.setHex( '0xbd1550' ) : this.material.color.setHex( '0xe97f02' );

            cubeIndex++;
            cube[cubeIndex] = this;
        }

        Cube.prototype.draw = function() {

            this.c.position.z >= this.maxLife ? this.c.position.z = -this.maxLife : this.c.position.y >= this.maxLife ? this.c.position.y = -this.maxLife : this.c.position.x >= this.maxLife ? this.c.position.x = -this.maxLife:null;

            this.id % 2 === 0 ? this.c.position.z += this.speed : this.id % 3 === 0 ? this.c.position.x += this.speed : this.c.position.y += this.speed;

            scene.add(this.c);
        }

        camera.position.z = 100;

        var ambient = new THREE.AmbientLight( 'white' ); // soft white light
        scene.add( ambient );

        // new Light(0, 0, 1000, 0xff0000);
        // new Light(0, 0, -1000, 0x009dff);

        function Light(x, y, z, color) {
            this.light = new THREE.DirectionalLight(color, 0.3);
            this.light.castShadow = true;
            this.light.shadowCameraVisible = true;
            // this.light.shadowCameraNear = 100;
            // this.light.shadowCameraFar = 200;
            // this.light.shadowCameraLeft = -20; // CHANGED
            // this.light.shadowCameraRight = 20; // CHANGED
            // this.light.shadowCameraTop = 20; // CHANGED
            // this.light.shadowCameraBottom = -20; // CHANGED

            this.light.position.set(x, y, z); // CHANGED
            scene.add(this.light);
            //            scene.add( new THREE.DirectionalLightHelper(this.light, 1) );
        }

        // renderer.shadowMapEnabled = true;
        // renderer.shadowMapSoft = false;


        for(var i = 0; i < cubeNmb; i += 1) {
            new Cube();
        }

        function animate() {

            //            camera.rotation.y  += .001;

            for(var i in cube) {
                cube[i].draw();
            }

            controls.update();
            requestAnimationFrame( animate );
            renderer.render( scene, camera );
        }

        animate();

        setTimeout(function() {
            renderer.setSize( container.offsetWidth, container.offsetHeight );
            container.insertBefore( renderer.domElement, document.querySelector('.text'));
        }, 200);

        window.addEventListener('resize', function() {
            camera.aspect = container.offsetWidth / container.offsetHeight;
            camera.updateProjectionMatrix();
            renderer.setSize( container.offsetWidth, container.offsetHeight );
        });
    }
    return {
        init:init
    }
}());
