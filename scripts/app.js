(() => {
  const form = document.querySelector(".form");
  const search = document.querySelector(".search");


  form.addEventListener('submit', e => {
    e.preventDefault();
    if(search.value && search.value != " ")
      window.open("https://duckduckgo.com/?q="+search.value.replace(/ /g, "+"),
                  "_blank");
      search.value = "";
  })

  window.addEventListener('click', e => {
    e.target.className === "search" ? e.target.focus() : search.blur();
  })

  canvas.init();

}).call();
