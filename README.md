DuckDuckTab
---
It's an HTML page made to replace new tab
There is a working searching bar with DuckDuckGo !bangs included :)

![screenshot.png](./screenshot.png)

---
Made with THREE.JS, vanilla js, css.

---
How to Install

```
git clone git@github.com:ziitrus/DuckDuckTab.git
```

```
chrome://extension

check developper mode

Load unpacked extension
```
